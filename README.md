BOSOM Calculator: A Breast Cancer Outcome - Survival Online Measurement Calculator using Data Mining and Predictive Modeling on SEER data
---

A project by Giltroy Meren

## Paper
The published paper for this application is [available here](http://cas.upm.edu.ph:8080/xmlui/handle/123456789/41).