package ph.edu.upm.agila.gtmeren.bosom.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.edu.upm.agila.gtmeren.bosom.domain.WekaData;
import ph.edu.upm.agila.gtmeren.bosom.service.CalcService;

@Controller
public class CalcController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private CalcService calcService;

	@RequestMapping(value = "/calc", method = RequestMethod.GET)
	public String showGet(@ModelAttribute("wekaData") WekaData wekaData,
			Model model, HttpServletRequest request,
			HttpServletResponse response) {
		model.addAttribute("title", "Calculator");
		model.addAttribute("pageName", "calc");
		model.addAttribute("pageTitleHeader", "BOSOM Calculator");
		model.addAttribute("pageTitleSubheader",
				"Evaluate your survival prediction");

		return "calc/form";
	}

	@RequestMapping(value = "/calc", method = RequestMethod.POST)
	public String showPost(
			@ModelAttribute("wekaData") @Valid WekaData wekaData,
			final BindingResult bindingResult, ModelMap model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		if (bindingResult.hasErrors()) {
			model.addAttribute("title", "Calculator");
			model.addAttribute("pageName", "calc");
			model.addAttribute("pageTitleHeader", "BOSOM Calculator");
			model.addAttribute("pageTitleSubheader",
					"Evaluate your survival prediction");
			model.addAttribute("pageTitleSubheader",
					"Evaluate your survival prediction");
			model.addAttribute("alertStrongContent",
					"You have errors in the form.");
			model.addAttribute("alertContent",
					"Please review the information you provided before submission.");
			model.addAttribute("alertType", "danger");

			return "calc/form";
		}

		model.addAttribute("title", "Results");
		model.addAttribute("pageName", "calc");
		model.addAttribute("pageTitleHeader", "Predictive results");
		model.addAttribute("pageTitleSubheader",
				"Our predictive model's interpretation of your survival.");
		model.addAttribute("isFlotUsed", true);

		logger.info("Form data:  " + wekaData + "\n");
		model.addAttribute("wekaData", wekaData);
		model.addAttribute("predictionsMap",
				calcService.evaluate(wekaData, request));

		model.addAttribute("pdfLocation",
				PdfController.getPdfFilePath(wekaData, request, response));

		return "calc/results";
	}

}
