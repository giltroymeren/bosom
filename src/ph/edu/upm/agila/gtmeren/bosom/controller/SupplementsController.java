package ph.edu.upm.agila.gtmeren.bosom.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SupplementsController {

	protected final static Log logger = LogFactory
			.getLog(SupplementsController.class);

	@RequestMapping(value = "/supplements", method = RequestMethod.GET)
	public String showSupplementsPage(ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		model.addAttribute("title", "Supplements");
		model.addAttribute("pageName", "supplements");
		model.addAttribute("pageTitleHeader", "Supplemental Links");
		model.addAttribute("pageTitleSubheader",
				"Local and international institutions and groups dedicated "
						+ "to breast cancer research and prevention.");

		return "supplements";
	}

}