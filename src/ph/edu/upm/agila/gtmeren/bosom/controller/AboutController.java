package ph.edu.upm.agila.gtmeren.bosom.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AboutController {

	@RequestMapping(value = { "/about", "/about/bosom" })
	public String showAboutBosomPage(ModelMap model) {
		model.addAttribute("title", "About - BOSOM Calculator");
		model.addAttribute("pageName", "about");
		model.addAttribute("pageTitleHeader", "Calculating Breast Cancer");
		model.addAttribute("pageTitleSubheader",
				"Learn more about the BOSOM Calculator and its components");
		return "about/bosom";
	}

	@RequestMapping(value = "/about/site")
	public String showAboutSitePage(ModelMap model) {
		model.addAttribute("title", "About - Site");
		model.addAttribute("pageName", "about");
		model.addAttribute("pageTitleHeader", "Site General Information");
		model.addAttribute("pageTitleSubheader",
				"Technical information and acknowledgements to the site's backbone technology");
		return "about/site";
	}

}
