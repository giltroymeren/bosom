package ph.edu.upm.agila.gtmeren.bosom.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import ph.edu.upm.agila.gtmeren.bosom.domain.WekaData;

public interface CalcService {

	public Map<String, Map<String, Map<String, Object>>> evaluate(
			WekaData wekaData, HttpServletRequest request);

}
