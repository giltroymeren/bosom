package ph.edu.upm.agila.gtmeren.bosom.service;

import ph.edu.upm.agila.gtmeren.bosom.domain.WekaData;
import weka.core.Instances;

public interface CalcArffService {

	public Instances getInstances(WekaData wekaData);

}