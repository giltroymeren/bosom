<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>

<c:set var="title" value="Something wrong happened" />
<c:set var="pageName" value="error" />

<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>

<div class="jumbotron jumbotron-error" id="page-exception">
    <div class="overlay">
        <h1>Something wrong happened.</h1>
        <p class="lead">
            We are genuinely sorry for this.
            Don't worry, it's on us. We'll fix it as soon as we can.
        </p>
        <p>
            Please e-mail the developer at <tt>gpmeren+bosom@up.edu.ph</tt> to report this occurrence.
            Kindly state what you were doing i.e., answering the form so we can find ans solve
            the problem in less time. 
        </p>
        <p>
            Please click <a href="/">this link</a> to go back to the home page.
        </p>
    </div>
</div>

<div class="row hide">

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>