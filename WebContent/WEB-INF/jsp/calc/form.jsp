<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/page-header.jsp"%>

<c:if test="${not empty alertContent}">
	<div class="col-lg-12 col-sm-12 col-xs-12">
		<div
			class="alert alert-<c:out value="${alertType}"/> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			<strong><c:out value="${alertStrongContent}" /></strong>
			<c:out value="${alertContent}" />
		</div>
	</div>
</c:if>

<div class="col-lg-5 col-sm-4 col-xs-12">
	<h4>Reminders</h4>
	<p>
	   In order to provide you with the predicted breast cancer survival,
	   the form provided in this page must be accomplished completely and correctly. 
	   If any alerts or error messages show after submitting, kindly follow their
	   instruction to successfully answer the form.
    </p>

	<p>
	   There are guides provided 
	   (seen as 
	   <span class="label label-info">
	       <span class="glyphicon glyphicon-info-sign"></span>
	       More info
	   </span>) 
	   beside each item to help you understand. Note that most of the terms provided
	   are in medical jargon - please ask a doctor for these values' definition.
	</p>
	
	<p>
	   The predicted survival provided by the BOSOM Calculator does not directly
	   correspond to a legitimate diagnosis. It is strongly advised to consult a 
	   doctor or cancer specialist to interpret and guide the patient regarding the 
	   relationships of the input fields and their values to the predictions.  
	</p>
	
</div>

<div class="col-lg-7 col-sm-8 col-xs-12 details">

	<h4>Please provide answers to the following items:</h4>
	
	<p>
	   Click on each item to either type in your answer or choose from the values provided.
	</p>

    <div class="panel panel-default">
        <form:form id="form-calc" modelAttribute="wekaData" method="POST" role="form" action="calc">
            <div class="panel-body">
            
                
                <c:set var="ageDiagNumErrors"><form:errors path="ageDiagNum"/></c:set>
                <c:set var="raceGroupErrors"><form:errors path="raceGroup"/></c:set>
                <c:set var="stage3Errors"><form:errors path="stage3"/></c:set>
                <c:set var="m3Errors"><form:errors path="m3"/></c:set>
                <c:set var="reasonNoCancerSurgErrors"><form:errors path="reasonNoCancerSurg"/></c:set>
                <c:set var="ext2Errors"><form:errors path="ext2"/></c:set>
                
                <div class="form-group
                    <c:if test="${not empty ageDiagNumErrors}">has-error</c:if>
                ">
                    <label for="form-ageDiagNum" class="block">
                        Age of patient in years <br/> 
                        at time of diagnosis (1 - 150 only)
                    </label>
                    <form:input class="form-control" path="ageDiagNum"
                        id="form-ageDiagNum" required="required" autocomplete="off"
                        min="1" max="150" pattern="\d+"/>
                    <form:errors path="ageDiagNum" element="p" cssClass="help-block bg-danger text-danger"/>
                </div>

                <div class="form-group
                    <c:if test="${not empty raceGroupErrors}">has-error</c:if>
                ">
                    <label for="form-raceGroup" class="block">Race of patient</label>
                    <form:select class="form-control" path="raceGroup"
                        id="form-raceGroup" required="required">
                        <form:option value=""></form:option>
                        <form:option value="Black">Black</form:option>
                        <form:option value="White">White</form:option>
                        <form:option value="Other">Otherwise</form:option>
                        <form:option value="Unknown">Unknown race</form:option>
                    </form:select>
                    <form:errors path="raceGroup" element="p" cssClass="help-block bg-danger text-danger"/>
                </div>

                <div class="form-group
                    <c:if test="${not empty stage3Errors}">has-error</c:if>
                ">
                    <label for="form-stage3" class="block">Stage of cancer (AJCC 6th Edition)</label>
                    <form:select class="form-control" path="stage3" id="form-stage3"
                        required="required">
                        <form:option value=""></form:option>
                        <form:option value="0">0</form:option>
                        <form:option value="I">I</form:option>
                        <form:option value="IIA">IIA</form:option>
                        <form:option value="IIB">IIB</form:option>
                        <form:option value="IIIA">IIIA</form:option>
                        <form:option value="IIIB">IIIB</form:option>
                        <form:option value="IIIC">IIIC</form:option>
                        <form:option value="IIINOS">IIINOS</form:option>
                        <form:option value="IV">IV</form:option>
                        <form:option value="UNK Stage">Unknown stage</form:option>
                    </form:select>
                    <form:errors path="stage3" element="p" cssClass="help-block bg-danger text-danger"/>
                </div>

                <div class="form-group
                    <c:if test="${not empty m3Errors}">has-error</c:if>
                ">
                    <label for="form-m3" class="block">Spread of metastasis</label>
                    <button type="button" class="btn btn-info btn-xs pull-right" 
                        data-toggle="modal" data-target="#modal-m3">
                        <span class="glyphicon glyphicon-info-sign"></span> More info 
                    </button>
                    <form:select class="form-control" path="m3" id="form-m3"
                        required="required">
                        <form:option value=""></form:option>
                        <form:option value="M0">M0 (No distant metastasis)</form:option>
                        <form:option value="M1">M1 (Distant metastasis)</form:option>
                        <form:option value="MX">MX (Distant metastasis cannot be assessed)</form:option>
                    </form:select>
                    <form:errors path="m3" element="p" cssClass="help-block bg-danger text-danger"/>
                </div>

                <div class="form-group
                    <c:if test="${not empty reasonNoCancerSurgErrors}">has-error</c:if>
                ">
                    <label for="form-reasonNoCancerSurg" class="block">
                        Details of cancer-directed surgery
                    </label>
                    <form:select class="form-control" path="reasonNoCancerSurg"
                        id="form-reasonNoCancerSurg" required="required">
                        <form:option value=""></form:option>
                        <form:option
                            value="Not performed, patient died prior to recommended surgery">
                            Not performed and patient died prior to recommended surgery
                        </form:option>
                        <form:option value="Not recommended">
                            Not recommended only
                        </form:option>
                        <form:option
                            value="Not recommended, contraindicated due to other conditions">
                            Not recommended and contraindicated due to other conditions
                        </form:option>
                        <form:option
                            value="Recommended but not performed, patient refused">
                             Recommended but not performed, patient refused
                        </form:option>
                        <form:option value="Recommended but not performed, unknown reason">
                            Recommended but not performed for unknown reasons
                        </form:option>
                        <form:option value="Recommended, unknown if performed">
                            Recommended but unknown if performed 
                        </form:option>
                        <form:option value="Surgery performed">
                            Surgery performed
                        </form:option>
                        <form:option
                            value="Unknown; death certificate or autopsy only case">
                            Unknown OR death certificate or autopsy-only case
                        </form:option>
                    </form:select>
                    <form:errors path="reasonNoCancerSurg" element="p" cssClass="help-block bg-danger text-danger"/>                
                </div>

                <div class="form-group
                    <c:if test="${not empty ext2Errors}">has-error</c:if>
                ">
                    <label for="form-ext2" class="block">Extension of primary tumor code</label>
                    <button type="button" class="btn btn-info btn-xs pull-right" 
                        data-toggle="modal" data-target="#modal-ext2">
                        <span class="glyphicon glyphicon-info-sign"></span> More info
                    </button>
                    <form:select class="form-control" path="ext2" id="form-ext2"
                        required="required">
                        <form:option value=""></form:option>
                        <form:option value="00">0</form:option>
                        <form:option value="05">5</form:option>
                        
                        <form:option value="10">10</form:option>
                        <form:option value="11">11</form:option>
                        <form:option value="13">13</form:option>
                        <form:option value="14">14</form:option>
                        <form:option value="15">15</form:option>
                        <form:option value="16">16</form:option>
                        <form:option value="17">17</form:option>
                        <form:option value="18">18</form:option>
                       
                        <form:option value="20">20</form:option>
                        <form:option value="21">21</form:option>
                        <form:option value="23">23</form:option>
                        <form:option value="24">24</form:option>
                        <form:option value="25">25</form:option>
                        <form:option value="26">26</form:option>
                        <form:option value="27">27</form:option>
                        <form:option value="28">28</form:option>
	                    
	                    <form:option value="30">30</form:option>
                        <form:option value="31">31</form:option>
                        <form:option value="33">33</form:option>
                        <form:option value="34">34</form:option>
                        <form:option value="35">35</form:option>
                        <form:option value="36">36</form:option>
                        <form:option value="37">37</form:option>
                        <form:option value="38">38</form:option>
	                        
	                    <form:option value="40">40</form:option>
                        <form:option value="50">50</form:option>
                        <form:option value="60">60</form:option>
                        <form:option value="70">70</form:option>
                        <form:option value="80">80</form:option>
                        <form:option value="85">85</form:option>
                        <form:option value="99">99</form:option>
                    </form:select>
                    <form:errors path="ext2" element="p" cssClass="help-block bg-danger text-danger"/>
                </div>

            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" data-toggle="modal"
                    data-target="#modal-submit" id="calc-btn-submit"
                    data-loading-text="<span class='glyphicon glyphicon-refresh'></span> 
                       Submitting your form ...">Submit</button>
                <button type="reset" class="btn btn-danger">Clear</button>
            </div>
    
            <!--  
            <div class="modal fade" id="modal-submit" tabindex="-1" role="dialog"
                aria-labelledby="submit-label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="submit-label">Confirm form
                                submission</h3>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure with the answers you provided in the form?
                                There could be items you missed or answered incorrectly.</p>

                            <p>
                                Click
                                <kbd>Submit</kbd>
                                to continue the form submission or
                                <kbd>Cancel</kbd>
                                to go back to the BOSOM page and check the answers.
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar" 
                                    role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="width: 0">
                                    <span class="sr-only">45% Complete</span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="submit-modal-form" type="button"
                                class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button id="modal-submit-btn" type="submit"
                                class="btn btn-primary"
                                data-loading-text="<span class='glyphicon glyphicon-refresh'></span> 
                                    Submitting your form...">
                                Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            -->
            
       </form:form>
    </div>   
	  
</div>

<%@ include file="/WEB-INF/jsp/calc/modals.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>