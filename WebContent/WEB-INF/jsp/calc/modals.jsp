<div class="modal fade" id="modal-m3" tabindex="-1" role="dialog" 
    aria-labelledby="modal-m3-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-m3-label">
                    Metastasis <br/>
                    <small>
                        <tt>Adjusted AJCC M 6th edition</tt>, SEER 18 
                    </small>
                </h4>
            </div>

            <div class="modal-body">
                The information below were taken from
                <a target="_blank" href="http://seer.cancer.gov/seerstat/variables/seer/ajcc-stage/6th/breast.html#m">
                    "Adjusted AJCC 6 M (1988+)", <em>Breast Schema for 1988+ based on AJCC 6th edition</em>
                </a>
                
                <hr/>
                
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><tt>M0</tt></td>
                                <td>No distant metastasis</td>
                            </tr>
                            <tr>
                                <td><tt>M1</tt></td>
                                <td>Distant metastasis</td>
                            </tr>
                            <tr>
                                <td><tt>MX</tt></td>
                                <td>Distant metastasis cannot be assessed</td>
                            </tr>
                        </tbody>        
                    </table>
                </div>      

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>     

<div class="modal fade" id="modal-ext2" tabindex="-1" role="dialog" 
    aria-labelledby="modal-ext2-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-ext2-label">
                    Extension of tumor <br/>
                    <small>
                        <tt>EOD 10 - extend (1988-2003)</tt>, SEER 18 
                    </small>
                </h4>
            </div>

            <div class="modal-body">
                The information below were taken from
                <a target="_blank" href="http://seer.cancer.gov/archive/manuals/EOD10Dig.pub.pdf">
                    "Breast Extension", <em>SEER Extent of Disease -- 1988: Codes and Coding Instructions, 1998</em>
                </a>
                (page 110).
                
                <hr/>
                
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><tt>00</tt></td>
                                <td>
                                    IN SITU: Noninfiltrating; intraductal <br/>
                                    WITHOUT infiltration; lobular neoplasia
                                </td>
                            </tr>
                            <tr>
                                <td><tt>05</tt></td>
                                <td>
                                    Paget's disease (WITHOUT underlying tumor)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>10</tt></td>
                                <td>
                                    Confined to breast tissue and fat including nipple and/or areola
                                </td>
                            </tr>
                            <tr>
                                <td><tt>11</tt></td>
                                <td>
                                    Entire tumor reported as invasive (no in situ component reported)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>13</tt></td>
                                <td>
                                    Invasive and in situ components present, size of invasive component stated and coded in tumor Size
                                </td>
                            </tr>
                            <tr>
                                <td><tt>14</tt></td>
                                <td>
                                     Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND in situ described as minimal (less than 25%)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>15</tt></td>
                                <td>
                                    Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND in situ described as extensive (25% or more)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>16</tt></td>
                                <td>
                                    Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND proportions of in situ and invasive not known
                                </td>
                            </tr>
                            <tr>
                                <td><tt>17</tt></td>
                                <td>
                                    Invasive and in situ components present, unknown size of tumor (Tumor Size coded <tt>999</tt>)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>18</tt></td>
                                <td>
                                    Unknown if invasive and in situ components present, unknown if tumor size represents mixed tumor or a "pure" tumor
                                </td>
                            </tr>
                            <tr>
                                <td><tt>20</tt></td>
                                <td>
                                    Invasion of subcutaneous tissue <br/>
                                    Skin infiltration of primary breast including skin of nipple and/or areola <br/>
                                    Local infiltration of dermal lymphatics adjacent to primary tumor involving skin by direct extension
                                </td>
                            </tr>
                            <tr>
                                <td><tt>21</tt></td>
                                <td>
                                    Entire tumor reported as invasive (no in situ component reported)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>23</tt></td>
                                <td>
                                    Invasive and in situ components present, size of invasive component stated and coded in tumor Size
                                </td>
                            </tr>
                            <tr>
                                <td><tt>24</tt></td>
                                <td>
                                     Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND in situ described as minimal (less than 25%)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>25</tt></td>
                                <td>
                                    Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND in situ described as extensive (25% or more)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>26</tt></td>
                                <td>
                                    Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND proportions of in situ and invasive not known
                                </td>
                            </tr>
                            <tr>
                                <td><tt>27</tt></td>
                                <td>
                                    Invasive and in situ components present, unknown size of tumor (Tumor Size coded <tt>999</tt>)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>28</tt></td>
                                <td>
                                    Unknown if invasive and in situ components present, unknown if tumor size represents mixed tumor or a "pure" tumor
                                </td>
                            </tr>
                            <tr>
                                <td><tt>30</tt></td>
                                <td>
                                    Invasion of (or fixation to) pectoral fascia or muscle; deep fixation; attachment or fixation to pectoral muscle or underlying tissue
                                </td>
                            </tr>
                            <tr>
                                <td><tt>31</tt></td>
                                <td>
                                    Entire tumor reported as invasive (no in situ component reported)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>33</tt></td>
                                <td>
                                    Invasive and in situ components present, size of invasive component stated and coded in tumor Size
                                </td>
                            </tr>
                            <tr>
                                <td><tt>34</tt></td>
                                <td>
                                     Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND in situ described as minimal (less than 25%)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>35</tt></td>
                                <td>
                                    Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND in situ described as extensive (25% or more)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>36</tt></td>
                                <td>
                                    Invasive and in situ components present, size of entire tumor coded in Tumor Size (size of invasive component not stated) AND proportions of in situ and invasive not known
                                </td>
                            </tr>
                            <tr>
                                <td><tt>37</tt></td>
                                <td>
                                    Invasive and in situ components present, unknown size of tumor (Tumor Size coded <tt>999</tt>)
                                </td>
                            </tr>
                            <tr>
                                <td><tt>38</tt></td>
                                <td>
                                    Unknown if invasive and in situ components present, unknown if tumor size represents mixed tumor or a "pure" tumor
                                </td>
                            </tr>
                            <tr>
                                <td><tt>40</tt></td>
                                <td>
                                    Invasion of (or fixation to) chest wall, ribs, intercostal or serratus anterior muscles
                                </td>
                            </tr>
                            <tr>
                                <td><tt>50</tt></td>
                                <td>
                                    Extensive skin involvement: Skin edema, peau d'orange, "pigskin", en cuirasse, lenticular nodule(s), inflammation of skin, erythema, ulceration of skin of breast, satellite nodule(s) in skin of primary breast
                                </td>
                            </tr>
                            <tr>
                                <td><tt>60</tt></td>
                                <td>
                                    (<tt>50</tt>) + (<tt>40</tt>) 
                                    <ul>
                                        <li>
                                            Extensive skin involvement: Skin edema, peau d'orange, “pigskin,” en cuirasse, lenticular nodule(s), inflammation of skin, erythema, ulceration of skin of breast, satellite nodule(s) in skin of primary breast 
                                        </li>
                                        <li>
                                            Invasion of (or fixation to) chest wall, ribs, intercostal or serratus anterior muscles
                                        </li>
                                    </ul> 
                                </td>
                            </tr>
                            <tr>
                                <td><tt>70</tt></td>
                                <td>
                                    Inflammatory carcinoma, incl. diffuse (beyond that directly overlying the tumor) dermal lymphatic permeation or infiltration
                                </td>
                            </tr>
                            <tr>
                                <td><tt>80</tt></td>
                                <td>
                                    FURTHER contiguous extension: Skin over sternum, upper abdomen, axilla or opposite breast
                                </td>
                            </tr>
                            <tr>
                                <td><tt>85</tt></td>
                                <td>
                                    Metastasis:
                                    <ul>
                                        <li>Bone, other than adjacent rib</li>
                                        <li>Lung</li>
                                        <li>Breast, contralateral - if stated as metastatic</li>
                                        <li>Adrenal gland</li>
                                        <li>Ovary</li>
                                        <li>Satellite nodule(s) in skin other than primary breast</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><tt>99</tt></td>
                                <td>
                                    UNKNOWN if extension or metastasis
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>     